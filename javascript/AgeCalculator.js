// String format used by the date picker e.g. "21 Feb 2005"
const DATE_FORMAT = 'DD MMM YYYY';


/**
 * Calculate the age in years, days, and hours & show on the webpage
 * 
 * @param {String} name e.g. "Jane Smith"
 * @param {String} dateOfBirthText e.g. "21 Feb 2005"
 */
function AgeCalculator(name, dateOfBirthText) {


    this.name = name;
    this.dateOfBirthText = dateOfBirthText;
    this.years;
    this.days;
    this.hours;


    /**
     * Validates the required fields and date format
     * Returns true if there's an error, false otherwise
     */
    this.validate = function () {
        var error = false;

        $('#name_help').text('');
        $('#date_of_birth_help').text('');

        if (!this.name) {
            error = true;
            $('#name_help').text('Field required');
        }
        if (!this.dateOfBirthText) {
            error = true;
            $('#date_of_birth_help').text('Field required');
        } else if (!moment(this.dateOfBirthText, DATE_FORMAT).isValid()) {
            error = true;
            $('#date_of_birth_help').text('Invalid date - please use the popup');
        } else if (moment(this.dateOfBirthText, DATE_FORMAT).isAfter(moment())) {
            error = true;
            $('#date_of_birth_help').text('Date of birth cannot be in the future');
        }

        return error;
    };


    /**
     * Calculate the age in years, days, hours
     */
    this.calculate = function () {
        var dateOfBirth = moment(this.dateOfBirthText, DATE_FORMAT);
        var now = moment();

        this.years = now.diff(dateOfBirth, 'years');
        this.days = now.subtract(this.years, 'years').diff(dateOfBirth, 'days');
        this.hours = now.subtract(this.days, 'days').diff(dateOfBirth, 'hours');
    };

    /**
     * Display the calculated age and keep a copy in "previous submissions" table
     */
    this.display = function () {
        var age = this.years + ' years, ' + this.days + ' days, ' + this.hours + ' hours';

        $('#result').show();
        $('#previous').show();
        $('#dob').text(this.dateOfBirthText);
        $('#years').text(this.years);
        $('#days').text(this.days);
        $('#hours').text(this.hours);
        $('tbody').prepend('<tr><td>' + this.name + '</td><td>' + age + '</td></tr>');
    };

    /**
     * Clear the form fields ready for the next entry
     */
    this.clearForm = function () {
        $('#name').val('');
        $('#date_of_birth').val('');
    };

}