# Age calculator

## Task

As a web user, I wish to find out my age accurate to number of hours so that I can impress my friends.

## Acceptance criteria

* A name can be entered
* My date of birth can be entered using a date picker
* My age in years days and hours is displayed
* Name and age of previous submissions is displayed